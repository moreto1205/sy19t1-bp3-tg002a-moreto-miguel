#include "Planet.h"
#include"TutorialApplication.h"
#include <iostream>
#include<OgreManualObject.h>
#include<OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;

Planet::Planet(SceneNode *node)
{
	mNode = node;

}


Planet::~Planet()
{
}

Planet *Planet::createPlanet(SceneManager* sceneManager, float size, ColourValue color)
{
	ManualObject *planet = sceneManager->createManualObject();
	planet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//BACK
	planet->position(-size, size, -size);
	planet->colour(color);	
	planet->position(size, -size, -size);
	planet->position(-size, -size, -size);

	planet->position(-size, size, -size);
	planet->position(size, size, -size);
	planet->position(size, -size, -size);


	//FRONT
	planet->position(-size, size, size);
	planet->position(-size, -size, size);
	planet->position(size, -size, size);
	
	planet->position(size, size, size);
	planet->position(-size, size, size);
	planet->position(size, -size, size);



	//TOP
	planet->position(-size, size, -size);
	planet->position(-size, size, size);
	planet->position(size, size, -size);

	planet->position(-size, size, size);
	planet->position(size, size, size);
	planet->position(size, size, -size);

	//BOTTOM
	planet->position(-size, -size, size);
	planet->position(-size, -size, -size);
	planet->position(size, -size, -size);

	planet->position(size, -size, size);
	planet->position(-size, -size, size);
	planet->position(size, -size, -size);

	//LEFT
	planet->position(-size, -size, size);
	planet->position(-size, size, -size);
	planet->position(-size, -size, -size);

	planet->position(-size, size, size);
	planet->position(-size, size, -size);
	planet->position(-size, -size, size);

	//RIGHT
	planet->position(size, size, -size);
	planet->position(size, -size, size);
	planet->position(size, -size, -size);

	planet->position(size, size, -size);
	planet->position(size, size, size);
	planet->position(size, -size, size);

	planet->end();

	SceneNode *node = sceneManager->getRootSceneNode()->createChildSceneNode();
	node->createChild();
	node->attachObject(planet);
	
	return new Planet(node);
}

void Planet::update(const FrameEvent & evt)
{

	float oldX = mNode->getPosition().x - mParent->mNode->getPosition().x;
	float oldZ = mNode->getPosition().z - mParent->mNode->getPosition().z;
	float newX = (oldX*Math::Cos((360/60 *mRevolutionSpeed)* evt.timeSinceLastFrame)) + (oldZ*Math::Sin((360/60 *mRevolutionSpeed)* evt.timeSinceLastFrame));
	float newZ = (oldX*-Math::Sin((360/60 *mRevolutionSpeed)*evt.timeSinceLastFrame)) + (oldZ*Math::Cos((360/60 *mRevolutionSpeed)* evt.timeSinceLastFrame));
	Degree Rotation = mLocalRotationSpeed;
	mNode->yaw(Radian((360/60 *Rotation)* evt.timeSinceLastFrame));
	mNode->setPosition(newX + mParent->mNode->getPosition().x, mNode->getPosition().y, newZ + mParent->mNode->getPosition().z);

	
	//	MATH::SIN/COS
	// center of earth (82,0,2.5)
	// sphere stacks and slices
	// https://stackoverflow.com/questions/47726891/how-to-revolve-an-object-around-another-object-c-and-ogre3d source for rotation
}



SceneNode & Planet::getNode()
{
	return *mNode;
}

void Planet::setParent(Planet *parent)
{
	mParent = parent;
}

Planet * Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = Radian(Degree(speed));
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = Radian(Degree(speed));
}
 