#pragma once
#include <iostream>
#include<string>
#include <vector>

using namespace std;

class Roll;
class Player
{
public:
	Player(string name, int hp, int crystals, int points, int pulls);
	~Player();
	void takeDamage(int value);
	void getPoints(int value);
	void getPulls(int value);
	void heal(int value);
	void crystal(int value);

	string getName();

	void printRoll();
	void printFinalRoll();

	string mName;
	int mHp;
	int mCrystals;
	int mPoints;
	int mPulls;
	int mSSR = 0;
	int mSR = 0;
	int mR = 0;
	int mCrystalspull= 0;
	int mHPotions = 0;
	int mBomb = 0;

	

};

