#include "GamblingGacha.h"
#include "Player.h"
#include "Bomb.h"
#include "Crystal.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "HealthPotion.h"
#include "Roll.h"
#include <iostream>
#include <string>

using namespace std;

GamblingGacha::GamblingGacha()
{
	this->payment = 5;

	mRolls.push_back(new R()); // 0
	mRolls.push_back(new SR());// 1
	mRolls.push_back(new SSR()); // 2
	mRolls.push_back(new HealthPotion()); //3
	mRolls.push_back(new Bomb()); // 4
	mRolls.push_back(new Crystal()); // 5
}

GamblingGacha::~GamblingGacha()
{
	for (int i = 0; i < mRolls.size(); i++)
		delete mRolls[i];
	mRolls.clear();
}

void GamblingGacha::GachaGame(Player* user)
{
	int randomizer = rand() % 100 + 1;

	user->mCrystals = user->mCrystals - payment;

	if (randomizer == 1)//SSR
	{
		mRolls[2]->cast(user);
	}
	else if (randomizer > 1 && randomizer < 11)//SR
	{
		mRolls[1]->cast(user);
	}
	else if (randomizer > 10 && randomizer < 51)//R
	{
		mRolls[0]->cast(user);
	}
	else if (randomizer > 50 && randomizer < 66)//HP
	{
		mRolls[3]->cast(user);
	}
	else if (randomizer > 65 && randomizer < 86)//Bomb
	{
		mRolls[4]->cast(user);
	}
	else if (randomizer > 85 && randomizer < 101)//Crystal
	{
		mRolls[5]->cast(user);
	}
}
