#include "Bomb.h"
#include <iostream>
#include <string>
#include "Roll.h"
using namespace std;

Bomb::Bomb()
{
	this->rDamage = 25;
	this->rPulls = 1;
}


Bomb::~Bomb()
{
}

void Bomb::cast(Player * user)
{
	cout << user->getName() << " ROLLED A BOMB AND DEALS 20 DAMAGE TO SELF" << endl;
	user->takeDamage(this->rDamage);
	user->getPulls(this->rPulls);
	user->mBomb += 1;
}
