#include "HealthPotion.h"
#include <iostream>
#include <string>

using namespace std;

HealthPotion::HealthPotion()
{
	this->rHp = 30;
	this->rPulls = 1;
}


HealthPotion::~HealthPotion()
{
}

void HealthPotion::cast(Player* user)
{
	cout << user->getName() << "ROLLED A HP POTION AND GAINS 30 HP" << endl;
	user->heal(this->rHp);
	user->getPulls(this->rPulls);
	user->mHPotions += 1;
}
