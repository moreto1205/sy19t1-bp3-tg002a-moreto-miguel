#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "Player.h"
#include "GamblingGacha.h"
#include "Roll.h"

using namespace std;

int main()
{
	srand(time(NULL));
	
	Player*user = new Player("USER",100, 100, 0,0);
	GamblingGacha* gameplay = new GamblingGacha;

	while (user->mHp > 0 && user->mCrystals > 0)
	{
		user->printRoll();

		gameplay->GachaGame(user);

		system("pause");
		system("cls");
	}

	if (user->mPoints >= 100)
	{
		cout << "YOU WIN THE GAME" << endl;
		user->printFinalRoll();
		system("pause");
	}

	else if (user->mHp <= 0)
	{
		cout << "YOU DIED AND LOST ;-; " << endl;
		user->printFinalRoll();
		system("pause");
	}
	else if (user->mCrystals <= 0)
	{
		cout << "YOU RAN OUT OF CRYSTALS AND LOST ;-;" << endl;
		user->printFinalRoll();
		system("pause");
	}
	
	system("pause");
	return 0;
}