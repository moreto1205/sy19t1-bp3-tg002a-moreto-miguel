#include "SSR.h"



SSR::SSR()
{
	this->rPoints = 50;
	this->rPulls = 1;
}


SSR::~SSR()
{
}

void SSR::cast(Player* user)
{
	cout << user->getName() << " RECEIVED AN SSR ITEM" << endl;
	user->getPoints(this->rPoints);
	user->getPulls(this->rPulls);
	user->mSSR += 1;
}
