#include "R.h"
#include <iostream>
#include <string>

using namespace std;


R::R()
{
	this->rPoints = 1;
	this->rPulls = 1;
}


R::~R()
{
}

void R::cast(Player * user)
{
	cout << user->getName() << " RECEIVED AN R ITEM" << endl;
	user->getPoints(this->rPoints);
	user->getPulls(this->rPulls);
	user->mR += 1;
}
