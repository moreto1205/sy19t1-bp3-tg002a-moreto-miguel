#include "Player.h"
#include "Bomb.h"
#include "Crystal.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "HealthPotion.h"
#include "Roll.h"
#include <string>
#include <vector>
#include <iostream>

using namespace std;


Player::Player(string name, int hp, int crystals, int points, int pulls)
{
	mName = name;
	mHp = hp;
	mCrystals = crystals;
	mPoints = points;
	mPulls = pulls;
}


Player::~Player()
{
	
}

void Player::takeDamage(int value)
{
	if (value > 0)
	{
		mHp -= value;
	}

}

void Player::getPoints(int value)
{
	if (value > 0)
	{
		mPoints += value;
	}
}

void Player::getPulls(int value)
{
	if (value > 0)
	{
		mPulls += value;
	}
}

void Player::heal(int value)
{
	if (value > 0)
	{
		mHp += value;
	}
}

void Player::crystal(int value)
{
	if (value > 0)
	{
		mCrystals += value;
	}
}

string Player::getName()
{
	return this->mName;
}


void Player::printRoll()
{
	cout << "NAME: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "CRYSTALS: " << mCrystals << endl;
	cout << "POINTS: " << mPoints << endl;
	cout << "PULLS: " << mPulls << endl;
}

void Player::printFinalRoll()
{
	cout << "HP: " << mHp << endl;
	cout << "CRYSTALS: " << mCrystals << endl;
	cout << "POINTS: " << mPoints << endl;
	cout << "PULLS: " << mPulls << endl;
	cout << "SSR's: " << mSSR << endl;
	cout << "SR's: " << mSR << endl;
	cout << "R's: " << mR << endl;
	cout << "BOMBS: " << mBomb << endl;
	cout << "HP POTIONS: " << mHPotions << endl;
	cout << "CRYSTALS PULLED: " << mCrystalspull << endl;
}


