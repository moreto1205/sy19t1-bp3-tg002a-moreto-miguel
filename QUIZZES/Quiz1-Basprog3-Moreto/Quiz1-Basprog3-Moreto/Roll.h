#pragma once
#include "Player.h"
#include <iostream>
#include<string>


using namespace std;

class Roll
{
public:
	Roll();
	~Roll();

	virtual void cast(Player* user);

protected:
	string rName;
	int mCrystals;
	int rPoints;
	int rDamage;
	int rHp;
	int rPulls;
	
};

