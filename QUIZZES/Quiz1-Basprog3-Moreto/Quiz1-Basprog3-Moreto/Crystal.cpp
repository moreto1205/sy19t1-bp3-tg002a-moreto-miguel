#include "Crystal.h"
#include <iostream>
#include <string>

using namespace std;

Crystal::Crystal()
{
	this->mCrystals = 15;
	this->rPulls = 1;
}

Crystal::~Crystal()
{
}

void Crystal::cast(Player* user)
{
	cout << user->getName() << " ROLLED AND GAINS 15 EXTRA CRYSTALS" << endl;
	user->crystal(this->mCrystals);
	user->getPulls(this->rPulls);
	user->mCrystalspull += 15;
}
