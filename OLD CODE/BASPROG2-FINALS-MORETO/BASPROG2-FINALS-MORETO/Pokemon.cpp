#include "Pokemon.h"
#include "Player.h"

#include<iostream>
#include <time.h>

using namespace std;

int randDamage = rand() % 15 + 10;
int randXP = rand() % 15 + 5;
int randLevel = rand() % 20 + 1;
int randencounterchance = rand() % 25 + 10;
int randcatchchance = rand() % 15 + 10;
int randpokeyen = rand() % 100 + 5;
int randHP = rand() % 100 + 1;
int maxhp = randHP;

Player* master = new Player("");
Pokemon* wild = new Pokemon(randHP, maxhp, randDamage, randLevel, randpokeyen, 50);

Pokemon::Pokemon()
{
	this->baseHP = 0;
	this->maxhp = 0;
	this->basedmg = 0;
	this->xp = 0;
	this->totalxp = 0;
	this->level = 0;
}
Pokemon::Pokemon(int baseHP, int hp, int basedmg, int level, int xp, int totalxp)
{
	this->baseHP = baseHP;
	this->maxhp = hp;
	this->basedmg = basedmg;
	this->xp = xp;
	this->totalxp = totalxp;
	this->level = level;
};
//Pokemon::Pokemon(string name, int baseHP, int hp, int basedmg, int level, int xp, int totalxp)
//{
//	this->name = name;
//	this->baseHP = baseHP;
//	this->maxhp = hp;
//	this->basedmg = basedmg;
//	this->xp = xp;
//	this->totalxp = totalxp;
//	this->level = level;
//};

void Pokemon::starter(int starterchoice)
{
	switch (starterchoice)
	{
	case 0:
		pokemonteam[0] = "BULBASAUR";
		break;

	case 1:
		pokemonteam[0] = "SQUIRTLE";
		break;

	case 2:
		pokemonteam[0] = "CHARMANDER";
		break;
	}


};

void Pokemon::stats(int baseHP, int hp, int basedmg, int xpgain, int totalxp, int level, int pokemonteamsize, int pokeyen, int starterlevel)
{

	for (int i = 0; i < starterlevel; i++)
	{
		baseHP = baseHP + (baseHP * 0.15);
		basedmg = basedmg + (basedmg * 0.10);
		xpgain = xpgain + (xpgain * 0.20);
		totalxp = totalxp + (totalxp * 0.20);
	}
	for (int i = 0; i < level; i++)
	{
		baseHP = baseHP + (baseHP * 0.15);
		basedmg = basedmg + (basedmg * 0.10);
		xpgain = xpgain + (xpgain * 0.20);
		totalxp = totalxp + (totalxp * 0.20);
		pokeyen = rand() % 100 + 5;
	}

	cout << "LEVEL:  " << level << endl;
	cout << pokemonteam[0] << endl;
	cout << "HP : " << baseHP << endl;
	cout << " BASE DAMAGE : " << basedmg << endl;
	cout << "XP EARNED :" << xp << "/" << totalxp << endl;

	for (int i = 1; i < pokemonteamsize; i++)
	{
		cout << "LEVEL:  " << level << endl;
		cout << pokemonteam[i] << endl;
		cout << "HP : " << baseHP << endl;
		cout << " BASE DAMAGE : " << basedmg << endl;
		cout << "XP EARNED :" << xp << "/" << totalxp << endl;
	}
	//
	//for (int i = 0; i < 6; i++)
	//{
	//	//cout << pokemonParty[i].name;
	//	cout << "HP: " << pokemonParty[i]->baseHP << " / " << pokemonParty[i]->maxhp;
	//	cout << "DAMAGE: " << pokemonParty[i]->baseHP;
	//	cout << "LEVEL: " << pokemonParty[i]->baseHP;
	//	cout << "XP: " << pokemonParty[i]->baseHP << " / " << pokemonParty[i]->totalxp;

	//}
}


//void Pokemon::copystats(Pokemon* wild)
//{
//this->baseHP = wild->baseHp;
//	this->basedmg = wild->basedmg;
//	this->level = wild->level;
//	this->totalxp = wild->totalxp;
//
//}
void Pokemon::encounter(int encounterchance, int baseHP, int level, int basedmg, int xp, int totalxp)
{
	cout << "A WILD : " << wildpokemon[randencounterchance] << " HAS APPEARED" << endl;
	cout << "LEVEL : " << level << endl;
	cout << "HP : " << baseHP << endl;


	cout << "WHAT WOULD YOU LIKE TO DO?" << endl;
	cout << R"( 1 - BATTLE
				2 - CATCH
				3 - RUN AWAY)" << endl;


}
void Pokemon::battle(int basedmg, int baseHP, int encounterchance, int totalxp, int xpgain, int pokemonteamsize, int randpokeyen)
{
	for (int i = 0; i < level; i++)
	{
		basedmg = basedmg + (basedmg * 0.10);
		baseHP = baseHP + (baseHP * 0.15);
		xpgain = xpgain + (xpgain * 0.20);
		totalxp = totalxp + (totalxp * 0.20);
	}

	Pokemon* enemypokemon =

		master->pokemonteam[6];

	for (int i = 1; i < pokemonteamsize; i++)
	{
		while (baseHP > 0);
		{
			cout << pokemonteam[pokemonteamsize] << " ATTACKED AND DEALT" << basedmg << " DAMAGE" << endl;
			cout << wildpokemon[randencounterchance] << " ATTACKED AND DEALT" << basedmg << " DAMAGE" << endl;
			baseHP = baseHP - basedmg;
			baseHP = baseHP - basedmg;

			cout << "LEVEL:  " << level << endl;
			cout << pokemonteam[i] << endl;
			cout << "HP : " << baseHP << endl;
			cout << " BASE DAMAGE : " << basedmg << endl;
			cout << "XP EARNED :" << xp << "/" << totalxp << endl;

			cout << "LEVEL:  " << level << endl;
			cout << wildpokemon[randencounterchance] << endl;
			cout << "HP : " << baseHP << endl;
			cout << " BASE DAMAGE : " << basedmg << endl;
			cout << "XP EARNED :" << xp << "/" << totalxp << endl;
			if (baseHP <= 0)
			{
				pokemonteamsize++;
			}
		}
		if (baseHP <= 0 && pokemonteamsize == 7)
		{
			cout << "ALL YOUR POKEMON HAS FAINTED" << endl;
		}

		if (xp == totalxp)
		{
			cout << "YOUR POKEMON HAS LEVELED UP TO LEVEL " << level << endl;
			level++;
		}
	}

}

void Pokemon::catching(int catchchance, int pokemonteamsize, int encouterchance, int pokeballs, int pokeyen)
{
	master->pokemonteam;

	if (catchchance > 9)
	{
		cout << "YOU CAUGHT" << wildpokemon[randencounterchance] << endl;
		for (int i = 1; i < pokemonteamsize; i++)
		{
			pokemonteamsize++;
			pokemonteam[i] = wildpokemon[randencounterchance];
			this->baseHP = wild->baseHP;
			this->basedmg = wild->basedmg;
			this->level = wild->level;
			this->maxhp = wild->maxhp;
			this->xp = wild->xp;
			this->totalxp = wild->totalxp;

		}
		pokeballs--;

		cout << "YOU ALSO FOUND " << randpokeyen << " POKEYEN" << endl;
		pokeyen = pokeyen + randpokeyen;

		if (pokeballs = 0)
		{
			cout << "YOU HAVE NO MORE POKEBALLS" << endl;
			cout << "PLEASE SELECT ANOTHER ACTION" << endl;
		}
	}
	else if (catchchance <= 9)
	{
		cout << "YOU WERE UNABLE TO CATCH " << wildpokemon[randencounterchance] << endl;
	}
}
/*


void Pokemon::CopyPokemon(Pokemon* wild) {
	this->name = wild->name;
	this->hp = wild->hp;
	etc.. etc..
}

	a wild pokemon appears
	random = rand() % min + max;

	Pokemon* newPokemon = pokemonList[random]
	pokemonList = new Pokemon("ALL ITS STATS")

	// if player used pokeball and captured successfully
	copy those stats

	pokemonParty["unusedSlot"] = copyPokemon(newPokemon);


	battle ->

	if (newPokemon.hp <= 0) {
		pokemonParty[current].xp += newPokemon.level * rand() % 100 + 1;
		break;
	}
*/