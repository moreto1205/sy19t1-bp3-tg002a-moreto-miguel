#pragma once
#include<string>
#include<time.h>

using namespace std;

class Pokemon
{
public:
	Pokemon();
	Pokemon(int baseHP, int maxhp, int basedmg, int level, int xp, int totalxp);
	Pokemon(string name, int baseHP, int maxhp, int basedmg, int level, int xp, int totalxp);

	string name;

	int baseHP;
	int basedmg;
	int maxhp;
	int damage;
	int xp;
	int totalxp;
	int xpgain;
	int level;
	int starterlevel = 5;
	int pokemonteamsize = 1;
	int encounterchance;
	int catchchance;
	int randHP = rand() % 100 + 1;
	int randDamage = rand() % 15 + 10;
	int randXP = rand() % 40 + 5;
	int randLevel = rand() % 20 + 1;
	int randencounterchance = rand() % 15 + 1;
	int randcatchchance = rand() % 15 + 1;
	int randpokeyen = rand() % 100 + 5;
	string pokemonteam[6];
	string starters[3]{ "BULBASAUR", "SQUIRTLE", "CHARMANDER" };
	string wildpokemon[15]{ "GIRATINA", "RAYQUAZA","SNIVY","WOOLOO", "EEVEE", "PIKACHU", "ROCKRUFF","MIMIKYU","LITLEO",
	"HOUNDOUR", "CHARMANDER", "SQUIRTLE", "BULBASAUR", "ELECTRIKE","SHINX" };

	void starter(int starterchoice);
	void stats(int baseHP, int maxhp, int basedmg, int xpgain, int totalxp, int level, int pokemonteamsize, int pokeyen, int starterlevel);
	void encounter(int encounterchance, int baseHP, int level, int basedmg, int xp, int totalxp);
	void battle(int baseHP, int basedmg, int encounterchance, int xpgain, int totalxp, int pokemonteamsize, int randompokeyen);
	void catching(int catchchance, int pokemonteamsize, int encouterchance, int pokeballs, int pokeyen);

};
