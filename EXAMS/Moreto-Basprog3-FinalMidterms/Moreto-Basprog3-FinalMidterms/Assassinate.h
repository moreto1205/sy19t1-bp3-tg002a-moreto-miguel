#pragma once
#include "Skills.h"
using namespace std;

class Unit;
class Assassinate :public Skills
{
public:
	Assassinate(string name, int value);
	~Assassinate();
	void activate(Unit* caster, vector<Unit*> Units);
};

