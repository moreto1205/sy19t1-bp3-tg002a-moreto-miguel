#pragma once
#include "Teams.h"
#include "Unit.h"

class Good : public Teams
{
public:
	Good(string name, int size);
	~Good();

	string name;
	int size;

	string getName();
	int getsize();

protected:
	string mName;
	int mSize;

};

