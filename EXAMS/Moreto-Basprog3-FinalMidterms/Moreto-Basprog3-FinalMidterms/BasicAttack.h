#pragma once
#include "Skills.h"
class Unit;
class BasicAttack :public Skills
{
public:
	BasicAttack(string name, int value);
	~BasicAttack();
	void activate(Unit* caster, vector<Unit*> Units);
};

