#include "Teams.h"

Teams::Teams(string name, int size)
{
	mName = name;
	mSize = size;
}

Teams::~Teams()
{
}

string Teams::getName()
{
	return mName;
}

int Teams::getSize()
{
	return mSize;
}
