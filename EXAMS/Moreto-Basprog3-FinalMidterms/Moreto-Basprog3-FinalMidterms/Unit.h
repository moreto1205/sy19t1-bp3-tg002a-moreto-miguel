#pragma once
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Teams;
class Skills;
class Unit
{
public:
	Unit(string name, int hp, int mp, int vit, int agi, int dex,
		int baseDmg,  string role, Teams* clan);
	~Unit();

	void printstats();
	void getDamage(int value, Unit* user, vector<Unit*> target);
	void takedamage(int value);
	void heal();
	void reduceMp(int value);
	void turnorder(vector <Unit*> allUnits);
	void attack(Unit* attacker, vector<Unit*> targets);
	void aiAttack(Unit* attacker, vector<Unit*> targets);

	string getName();
	string getClass();
	int getBaseDmg();
	int getVit();
	int getHp();
	int getDex();
	int getAgi();
	int getBaseHp();
	int getBaseMp();

	bool isAlive();
	bool useSkill(Skills* skill);
private:
	string mName;
	int mHp;
	int mMp;
	int mVit;
	int mAgi;
	int mDex;
	int mBaseDmg;
	int mbaseMp;
	int mbaseHp;
	string mRole;
	vector <Skills*> mSkills;
	vector <Unit*> allUnits;
};

