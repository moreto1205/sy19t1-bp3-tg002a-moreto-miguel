#include "Assassinate.h"

Assassinate::Assassinate(string name, int value):Skills(name, value)
{
	this->mValue = 10;
}

Assassinate::~Assassinate()
{
}

void Assassinate::activate(Unit* caster, vector<Unit*> Units)
{
	for (int i = 0; i < 3; i++)
	{
		if (Units[i]->getBaseHp() < (Units[i]->getBaseHp() * .5))
		{
			cout << caster->getName() << "USED ASSASSINATE ON" << Units[i]->getName();
			int damagetaken = caster->getDamage * 2.2;
			Units[i]->takedamage(damagetaken);
			caster->reduceMp;
		}
	}
}
