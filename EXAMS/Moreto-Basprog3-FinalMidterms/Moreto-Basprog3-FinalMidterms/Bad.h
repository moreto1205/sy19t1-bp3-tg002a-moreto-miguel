#pragma once
#include "Teams.h"
#include "Unit.h"

class Bad : public Teams
{
public:
	Bad(string name, int size);
	~Bad();

	string name;
	int size;

	string getName();
	int getsize();

protected:
	string mName;
	int mSize;

};
