#pragma once
#include <string>
using namespace std;
class Character;

class Teams
{
public:
	Teams(string name, int size);
	~Teams();

	string getName();
	int getSize();

protected:
	string mName;
	int mSize;
};

