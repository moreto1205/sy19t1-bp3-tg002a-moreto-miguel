#pragma once
#include "Unit.h"
#include "Heal.h"
#include "Shockwave.h"
#include "Assassinate.h"

class Skills
{
public:
	Skills(string name, int value);
	~Skills();

	virtual void activate(Unit* caster, vector<Unit*> allUnits);

	string getname();
	int getMpcost();
protected:
	string mName;
	int mValue;

};
