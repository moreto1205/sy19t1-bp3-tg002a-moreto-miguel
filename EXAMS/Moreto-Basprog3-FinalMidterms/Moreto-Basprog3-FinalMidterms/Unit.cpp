#include "Unit.h"
#include <string>
#include <iostream>
#include "Assassinate.h"
#include "Shockwave.h"
#include "BasicAttack.h"
#include "Heal.h"
#include <vector>
#include "Skills.h"
#include "Teams.h"

using namespace std;

Unit::Unit(string name, int hp, int mp, int vit, int agi, int dex, int baseDmg,
	 string role, Teams* clan)
{
	mName = name;
	mHp = hp;
	mMp = mp;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
	mBaseDmg = baseDmg;
	mRole = role;

	allUnits.push_back(new Unit("FRIENDLY ASSASSIN", 100, 100, 100, 100, 100, 100, "ASSASSIN", new Teams("GOOD", 0)));
	allUnits.push_back(new Unit("FRIENDLY WARRIOR", 100, 100, 100, 100, 100, 100, "WARRIOR", new Teams("GOOD", 0)));
	allUnits.push_back(new Unit("FRIENDLY MAGE", 100, 100, 100, 100, 100, 100, "MAGE", new Teams("GOOD", 0)));
	allUnits.push_back(new Unit("ENEMY ASSASSIN", 100, 100, 100, 100, 100, 100, "ASSASSIN", new Teams("BAD", 0)));
	allUnits.push_back(new Unit("ENEMY WARRIOR", 100, 100, 100, 100, 100, 100, "WARRIOR", new Teams("BAD", 0)));
	allUnits.push_back(new Unit("ENEMY MAGE", 100, 100, 100, 100, 100, 100, "MAGE", new Teams("BAD", 0)));
	
	if (role == "WARRIOR")
	{
		mSkills.push_back(new BasicAttack("SLASH", 0));
		mSkills.push_back(new Shockwave("SHOCKWAVE", 10));
	}

	else if (role == "ASSASSIN")
	{
		mSkills.push_back(new BasicAttack("BACKSTAB", 0));
		mSkills.push_back(new Assassinate("ASSASSINATE", 0));
	}

	else if (role == "MAGE")
	{
		mSkills.push_back(new BasicAttack("ARCANE MISSILE", 0));
		mSkills.push_back(new Heal("HEAL", 10));
	}



}

Unit::~Unit()
{
	for (int i = 0; i < mSkills.size(); i++)
		{
			delete mSkills[i];
		}
	mSkills.clear();
}

void Unit::printstats()
{
	cout << "NAME: " << mName << endl;
	cout << "CLASS: " << mRole << endl;
	cout << "HP: " << mbaseHp << "/" << mHp << endl;
	cout << "MP: " << mbaseMp << "/" << mMp << endl;
	cout << "POW: " << mBaseDmg << endl;
	cout << "AGI: " << mAgi << endl;
	cout << "DEX: " << mDex << endl;
	cout << "VIT: " << mVit << endl;
}

void Unit::getDamage(int value, Unit* user, vector<Unit*> target)
{
	int i = 0;
	float bonusDamage;
	float damage = (user->getBaseDmg - target[i]->getVit)* bonusDamage;

	if (user->getClass == "MAGE" && target[i]->getClass == "WARRIOR")
	{
		bonusDamage = 1.5;
	}

	else if (user->getClass == "ASSASSIN" && target[i]->getClass == "MAGE")
    {
		bonusDamage = 1.5;
	}
	
	else if (user->getClass == "WARRIOR" && target[i]->getClass == "ASSASSIN")
	{
		bonusDamage = 1.5;
	}

	else
	{
		bonusDamage = 1;
	}

}

void Unit::takedamage(int value)
{
	if (value > 0)
	{
		mbaseHp -= value;

		if (mbaseHp < 0)
			mbaseHp = 0;
	}
}

void Unit::heal()
{
	int value = (mHp * 0.3);
	mbaseHp += value;
}


void Unit::reduceMp(int value)
{
	if (value > 0)
	{
		mbaseMp -= value;

		if (mbaseMp < 0)
			mbaseMp = 0;
	}
}

void Unit::turnorder(vector <Unit*> allUnits)
{
	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < (6 - i + 1); j++)
		{
			if (allUnits[i]->getAgi > allUnits[i]->getAgi)
			{
				swap(allUnits[i], allUnits[j]);
			}
		}
	}
	cout << "TURN ORDER: " << endl;
	for (int i = 0; i < 6; i++)
	{
		cout << "# " << i << allUnits[i]->getName << endl;
	}
}

void Unit::attack(Unit* attacker, vector<Unit*> targets)
{
	int select;
	cout << "SELECT AN ACTION" << endl;
	attacker->printstats();
	cout << " 1 " << mSkills[0]->getname << "MP COST: " << mSkills[0]->getMpcost << endl;
	cout << " 2 " << mSkills[1]->getname << "MP COST: " << mSkills[1]->getMpcost << endl;

	cin >> select;

	if (select == 1)
	{
		mSkills[0]->activate(attacker, targets);
	}
	else if (select == 2)
	{
		mSkills[1]->activate(attacker, targets);
	}
}

void Unit::aiAttack(Unit* attacker, vector<Unit*> targets)
{
	int select = rand()  % 2 + 1;
	
	if (select == 1)
	{
		mSkills[0]->activate(attacker, targets);
	}
	else if (select == 2)
	{
		mSkills[1]->activate(attacker, targets);
	}
}


string Unit::getName()
{
	return this->mName;
}

string Unit::getClass()
{
	return this->mRole;
}

int Unit::getBaseDmg()
{
	mBaseDmg = mBaseDmg + (rand() % 20);
	return this->mBaseDmg;
}

int Unit::getVit()
{
	return this->mVit;
}

int Unit::getHp()
{
	return this->mHp;
}

int Unit::getDex()
{
	return this->mDex;
}

int Unit::getAgi()
{
	return this->mAgi;
}

int Unit::getBaseHp()
{
	return this->mbaseHp;
}

int Unit::getBaseMp()
{
	return this->mbaseMp;
}

bool Unit::isAlive()
{
	if (mbaseHp > 0)
		return true;
	else
	return false;
}

bool Unit::useSkill(Skills* skill)
{
	if (mbaseMp > skill->getMpcost())
	{
		return true;
	}
	else 
	return false;
}
