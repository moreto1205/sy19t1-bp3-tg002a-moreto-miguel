#pragma once
#include "Skills.h"
using namespace std;

class Unit;
class Shockwave : public Skills
{
public:
	Shockwave(string name, int value);
	~Shockwave();
	void activate(Unit* caster, vector<Unit*> allUnits);
};

