#include "Shockwave.h"

Shockwave::Shockwave(string name, int value):Skills(name, value)
{
	this->mValue = 10;
}

Shockwave::~Shockwave()
{
}

void Shockwave::activate(Unit* caster, vector<Unit*> allUnits)
{
	cout << caster->getName() << "USED SHOCKWAVE" << endl;
	int damagetaken = caster->getDamage * 0.9;
	for (int i = 0; i < 3; i++)
	{
		allUnits[i]->takedamage(damagetaken);
	}
	caster->reduceMp;
}
