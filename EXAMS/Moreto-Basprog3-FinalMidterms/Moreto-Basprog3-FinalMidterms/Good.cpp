#include "Good.h"
#include "Unit.h"

Good::Good(string name, int size) : Teams(name, size)
{
	mName = name;
	mSize = size;
}

Good::~Good()
{
}

string Good::getName()
{
	return mName;
}

int Good::getsize()
{
	return mSize;
}
