#include "BasicAttack.h"
#include <time.h>
#include <string>
#include <iostream>

using namespace std;

BasicAttack::BasicAttack(string name, int value) :Skills(name, value)
{
	this->mValue = 0;
}


BasicAttack::~BasicAttack()
{
}

void BasicAttack::activate(Unit* caster, vector<Unit*> Units)
{
	cout << caster->getName() << "USED A BASIC ATTACK" << endl;

	int randunit = rand() % 3 + 1;
	int critchance = rand() % 100 + 1;
	int hitchance = rand() % ((caster->getDex / Units[randunit]->getAgi) * 100);
	int roll;
	if (hitchance < 20)
	{
		roll = 20;
	}
	else if (hitchance > 80)
	{
		roll = 80;
	}
	else
	{
		roll = 100 - hitchance;
	}

	if (hitchance < roll)
	{

		if (critchance > 80)
		{
			cout << "IT'S A CRITICAL HIT!" << endl;
			int damagetaken = caster->getDamage * 1.2;
			Units[randunit]->takedamage(damagetaken);
		}

		else
		{
			cout << "YOU ATTACKED" << endl;
			int damagetaken = caster->getDamage;
			Units[randunit]->takedamage(damagetaken);
		}

	}

	else
	{
		cout << "YOU MISSED" << endl;
	}
}


//hit% = (DEX of attacker / AGI of defender) * 100

//damage = (baseDamage of attacker - VIT of defender) * bonusDamage
//baseDamage = Randomed POW * damageCoefficient

