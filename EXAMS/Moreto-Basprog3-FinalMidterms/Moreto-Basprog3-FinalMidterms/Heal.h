#pragma once
#include "Skills.h"
using namespace std;

class Unit;
class Heal : public Skills
{
public:
	Heal(string name, int value);
	~Heal();
	void activate(Unit* caster, vector<Unit*> allUnits);
};
