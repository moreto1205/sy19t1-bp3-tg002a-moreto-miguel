#include "Planet.h"
#include"TutorialApplication.h"
#include <iostream>
#include<OgreManualObject.h>
#include<OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;

Planet::Planet(SceneNode *node)
{
	mNode = node;
}


Planet::~Planet()
{
}

Planet *Planet::createPlanet(SceneManager* sceneManager, bool lighting, std::string matName, const float r, const int nRings , const int nSegments , ColourValue(color))
{
	ManualObject * planet = sceneManager->createManualObject();
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(matName, "General");
	myManualObjectMaterial->setReceiveShadows(false);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(lighting);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(color);
	myManualObjectMaterial->setColourWriteEnabled(true);

	// code from sample code provided

	planet->begin(matName, RenderOperation::OT_TRIANGLE_LIST);
	planet->colour(color);

	float fDeltaRingAngle = (Math::PI / nRings);
	float fDeltaSegAngle = (2 * Math::PI / nSegments);
	unsigned short wVerticeIndex = 0;

	// Generate the group of rings for the sphere
	for (int ring = 0; ring <= nRings; ring++) {
		float r0 = r * sinf(ring * fDeltaRingAngle);
		float y0 = r * cosf(ring * fDeltaRingAngle);

		// Generate the group of segments for the current ring
		for (int seg = 0; seg <= nSegments; seg++) {
			float x0 = r0 * sinf(seg * fDeltaSegAngle);
			float z0 = r0 * cosf(seg * fDeltaSegAngle);

			// Add one vertex to the strip which makes up the sphere
			planet->position(x0, y0, z0);
			planet->normal(Vector3(x0, y0, z0).normalisedCopy());
			planet->textureCoord((float)seg / (float)nSegments, (float)ring / (float)nRings);

			if (ring != nRings) {
				// each vertex (except the last) has six indicies pointing to it
				planet->index(wVerticeIndex + nSegments + 1);
				planet->index(wVerticeIndex);
				planet->index(wVerticeIndex + nSegments);
				planet->index(wVerticeIndex + nSegments + 1);
				planet->index(wVerticeIndex + 1);
				planet->index(wVerticeIndex);
				wVerticeIndex++;
			}
		}; 
	} 
	planet->end();

	SceneNode *node = sceneManager->getRootSceneNode()->createChildSceneNode();
	node->createChild();
	node->attachObject(planet);

	return new Planet(node);

	// code taken from http://wiki.ogre3d.org/tiki-pagehistory.php?page=ManualSphereMeshes&source=0
}

void Planet::lighting(SceneManager* sceneManager)
{
	Light * sunLight = sceneManager->createLight();
	sunLight->setType(Light::LightTypes::LT_POINT);
	sunLight->setPosition(Vector3(0,0,0));
	sunLight->setDiffuseColour(ColourValue(1.0F, 0.75F, 0.75F));
	sunLight->setAttenuation(3250, 1.0, 0.0014, 0.000007);
	sunLight->setSpecularColour(ColourValue(10.0f, 10.0f, 10.0f));
}

void Planet::update(const FrameEvent & evt)
{

	float oldX = mNode->getPosition().x - mParent->mNode->getPosition().x;
	float oldZ = mNode->getPosition().z - mParent->mNode->getPosition().z;
	float newX = (oldX*Math::Cos((360/60 *mRevolutionSpeed)* evt.timeSinceLastFrame)) + (oldZ*Math::Sin((360/60 *mRevolutionSpeed)* evt.timeSinceLastFrame));
	float newZ = (oldX*-Math::Sin((360/60 *mRevolutionSpeed)*evt.timeSinceLastFrame)) + (oldZ*Math::Cos((360/60 *mRevolutionSpeed)* evt.timeSinceLastFrame));
	Degree Rotation = mLocalRotationSpeed;
	mNode->yaw(Radian((360/60 *Rotation)* evt.timeSinceLastFrame));
	mNode->setPosition(newX + mParent->mNode->getPosition().x, mNode->getPosition().y, newZ + mParent->mNode->getPosition().z);

	//	MATH::SIN/COS
	// center of earth (82,0,2.5)
	// sphere stacks and slices
	// https://stackoverflow.com/questions/47726891/how-to-revolve-an-object-around-another-object-c-and-ogre3d source for rotation
}

SceneNode & Planet::getNode()
{
	return *mNode;
}

void Planet::setParent(Planet *parent)
{
	mParent = parent;
}

Planet * Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = Radian(Degree(speed));
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = Radian(Degree(speed));
}
 