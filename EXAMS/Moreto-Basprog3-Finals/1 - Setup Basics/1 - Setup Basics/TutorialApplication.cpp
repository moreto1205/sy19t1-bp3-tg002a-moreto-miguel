/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include<OgreManualObject.h>	
#include "Planet.h"
#include <vector>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

void TutorialApplication::lighting()
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	

	//PLANETS

	Planet* Sun = Planet::createPlanet(mSceneMgr, false, "mSun",20,16,16,ColourValue(1, 1, 0));
	Sun->getNode().setPosition(0, 0, 0); 
	Sun->setLocalRotationSpeed(5);
	Sun->setParent(Sun);
	Sun->lighting(mSceneMgr);

	Planet* Mercury = Planet::createPlanet(mSceneMgr, true, "mMercury",1.5, 16, 16, ColourValue(0.82f, 0.7f, 0.54f));
	Mercury->getNode().setPosition(57.9, 0, 0);
	Mercury->setLocalRotationSpeed(5);
	Mercury->setRevolutionSpeed(1 * 4.14);
	Mercury->setParent(Sun);

	Planet* Venus = Planet::createPlanet(mSceneMgr, true, "mVenus", 2.5, 16, 16, ColourValue(0.93, 0.9f, 0.67f));
	Venus->getNode().setPosition(108.2, 0, 0);
	Venus->setLocalRotationSpeed(5);
	Venus->setRevolutionSpeed(1 * 1.62);
	Venus->setParent(Sun);

	Planet* Earth= Planet::createPlanet(mSceneMgr, true,"mEarth", 5, 16, 16, ColourValue::Blue);
	Earth->getNode().setPosition(149.6,0,0);
	Earth->setLocalRotationSpeed(5);
	Earth->setRevolutionSpeed(1);
	Earth->setParent(Sun);

	Planet* Mars = Planet::createPlanet(mSceneMgr, true, "mMars", 4, 16, 16, ColourValue(0.71f, 0.25f, 0.05f));
	Mars->getNode().setPosition(227.9, 0, 0);
	Mars->setLocalRotationSpeed(5);
	Mars->setRevolutionSpeed(1 * 0.53);
	Mars->setParent(Sun);
	
	Planet* Moon = Planet::createPlanet(mSceneMgr, true, "mMoon",1, 16, 16, ColourValue(0.7f, 0.7f, 0.7f));
	Moon->setParent(Earth);
	Moon->getNode().setPosition(Moon->getParent()->getNode().getPosition().x + 25, 0, 0);
	Moon->setLocalRotationSpeed(5);
	Moon->setRevolutionSpeed(20);

	mPlanets.push_back(Sun);
	mPlanets.push_back(Mercury);
	mPlanets.push_back(Venus);
	mPlanets.push_back(Earth);
	mPlanets.push_back(Mars);
	mPlanets.push_back(Moon);

	//1 = 365
	//365 / X-> days of planet 
	// multiplier * 1
}

bool TutorialApplication::frameStarted(const FrameEvent &evt)
{
	for (int i = 0; i < mPlanets.size(); i++)
	{
		mPlanets.at(i)->update(evt);
	}

	return true;
}




//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
