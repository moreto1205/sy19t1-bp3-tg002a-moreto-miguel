#pragma once
#include "TutorialApplication.h"
#include<OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>
#include <vector>

using namespace std; 

class Planet
{
public:
	Planet(SceneNode *node);
	~Planet();

	static Planet* createPlanet(SceneManager* sceneManager, bool lighting, std::string strName, const float r, const int nRings , const int nSegments , ColourValue(color));
	void lighting(SceneManager* sceneManager);
	void update(const FrameEvent & evt);
			
	SceneNode &getNode();
	void setParent(Planet *parent);
	Planet * getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);

	
private:
	SceneNode *mNode;
	Planet *mParent;
	std::vector<Planet*> mPlanets;
	Radian mLocalRotationSpeed;
	Radian mRevolutionSpeed;

};

