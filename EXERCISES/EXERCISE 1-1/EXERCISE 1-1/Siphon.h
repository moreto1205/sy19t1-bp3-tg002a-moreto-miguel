#pragma once

#include<iostream>
#include<string>
#include "Skill.h"
#include "Weapon.h"

class Siphon :
	public Skill
{
public:
	Siphon();
	Siphon(string skillname, int weapondmg, int MP, Character* player, Character* enemy, Weapon* equip);
};

