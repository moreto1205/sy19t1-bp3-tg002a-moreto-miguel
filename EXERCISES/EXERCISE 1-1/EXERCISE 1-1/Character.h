#pragma once
#include<iostream>
#include<string>
#include "Weapon.h"
#include "DoppelBlade.h"
#include "Vengeance.h"
#include "Siphon.h"

using namespace std;

class Character
{
public:
	Character();
	Character(string name, int HP, int MP);
	string name;
	int HP;
	int MP;
	string skillname;
	int skilldmg;
	Weapon* equipweapon;
	int critchance;
	int choice = rand() % 3 + 1;
	int clonechoice = rand() % 3 + 1;

	void intro(string name);

	void stats();

	void battle(int choice, Character* enemy, Character* player, Weapon* equip, int clonechoice);

private:
	DoppelBlade* skill1 = new DoppelBlade;
	DoppelBlade* skill2 = new DoppelBlade();
	DoppelBlade* skill3 = new DoppelBlade;

};