#pragma once
#include<iostream>
#include<string>

using namespace std;

class Weapon
{
public:
	Weapon();
	Weapon(string weaponname, int weapondmg);

	string weaponname;
	int weapondmg;
};
