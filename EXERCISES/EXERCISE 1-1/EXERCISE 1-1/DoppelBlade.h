#pragma once

#include<iostream>
#include<string>
#include "Skill.h"

class DoppelBlade :
	public Skill
{
public:
	DoppelBlade();
	DoppelBlade(string skillname, int weapondmg, int MP, Character* player, Character* enemy, Weapon* equip);

};


