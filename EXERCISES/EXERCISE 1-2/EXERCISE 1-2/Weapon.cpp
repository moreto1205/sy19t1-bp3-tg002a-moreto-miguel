#include "Weapon.h"
#include <iostream>
#include <string>

using namespace std;

Weapon::Weapon()
{
	this->weaponname = "SWORD";
	this->weapondmg = 20;
}

Weapon::Weapon(string weaponname, int weapondmg)
{
	this->weaponname = weaponname;
	this->weapondmg = weapondmg;
}
