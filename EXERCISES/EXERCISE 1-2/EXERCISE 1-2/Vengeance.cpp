#include "Vengeance.h"
#include<iostream>
#include<string>
#include"Character.h"
#include "Weapon.h"

using namespace std;

Vengeance::Vengeance()
{
}

Vengeance::Vengeance(string skillname, int weapondmg, int MP, Character* player, Character* enemy, Weapon* equip)
{
	equip->weapondmg = ((player->HP * 0.25) * 2) + equip->weapondmg;
	player->HP = player->HP - (player->HP * 0.25);
	enemy->HP = enemy->HP - equip->weapondmg;
	player->MP = player->MP - 10;
}

