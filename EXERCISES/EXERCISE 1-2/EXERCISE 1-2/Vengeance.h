#pragma once
#include<iostream>
#include<string>
#include "Skill.h"

class Vengeance:
	public Skill
{
public:
	Vengeance();
	Vengeance(string skillname, int weapondmg, int MP, Character* player, Character* enemy, Weapon* equip);
};

