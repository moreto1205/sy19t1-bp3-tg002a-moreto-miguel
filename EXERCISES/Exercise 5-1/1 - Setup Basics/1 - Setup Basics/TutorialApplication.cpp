/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include<OgreManualObject.h>	

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	//std::vector<Planet*> mPlanets;


}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *object = mSceneMgr->createManualObject();

	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	Light * spotLight = mSceneMgr->createLight("SpotLight");
	spotLight->setDiffuseColour(ColourValue(1.0F, 1.0F, 0.0F));
	spotLight->setPosition(Vector3(0, 0, 0));
	spotLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
	{
	
		//BACK

		//Triangle 1
		object->position(-5, 5, -5);
		object->normal(Vector3(0, 0, -1));
		object->position(5, -5, -5);
		object->normal(Vector3(0, 0, -1));
		object->position(-5, -5, -5);
		object->normal(Vector3(0, 0, -1));

		//Triangle 2
		object->position(-5, 5, -5);
		object->normal(Vector3(0, 0, -1));
		object->position(5, 5, -5);	
		object->normal(Vector3(0, 0, -1));
		object->position(5, -5, -5);
		object->normal(Vector3(0, 0, -1));
	}

	{
		//FRONT

		//Triangle 1
		object->position(-5, 5, 5);
		object->normal(Vector3(0, 0, 1));
		object->position(-5, -5, 5);
		object->normal(Vector3(0, 0, 1));
		object->position(5, -5, 5);
		object->normal(Vector3(0, 0, 1));

		//Triangle 2
		object->position(5, 5, 5);
		object->normal(Vector3(0, 0, 1));
		object->position(-5, 5, 5);
		object->normal(Vector3(0, 0, 1));
		object->position(5, -5, 5);
		object->normal(Vector3(0, 0, 1));
	}

	{
		//TOP

		//Triangle 1
		object->position(-5, 5, -5);
		object->normal(Vector3(0, 1, 0));
		object->position(-5, 5, 5);
		object->normal(Vector3(0, 1, 0));
		object->position(5, 5, -5);
		object->normal(Vector3(0, 1, 0));

		//Triangle 2
		object->position(-5, 5, 5);
		object->normal(Vector3(0, 1, 0));
		object->position(5, 5, 5);
		object->normal(Vector3(0, 1, 0));
		object->position(5, 5, -5);
		object->normal(Vector3(0, 1, 0));
	}

	{
		//BOTTOM

		//Triangle 1
		object->position(-5, -5, 5);
		object->normal(Vector3(0, -1, 0));
		object->position(-5, -5, -5);
		object->normal(Vector3(0, -1, 0));
		object->position(5, -5, -5);
		object->normal(Vector3(0, -1, 0));

		//Triangle 2
		object->position(5, -5, 5);
		object->normal(Vector3(0, -1, 0));
		object->position(-5, -5, 5);
		object->normal(Vector3(0, -1, 0));
		object->position(5, -5, -5);
		object->normal(Vector3(0, -1, 0));
	}

	{
		//LEFT

		//Triangle 1
		object->position(-5, -5, 5);
		object->normal(Vector3(-1, 0, 0));
		object->position(-5, 5, -5);
		object->normal(Vector3(-1, 0, 0));
		object->position(-5, -5, -5);
		object->normal(Vector3(-1, 0, 0));

		//Triangle 2
		object->position(-5, 5, 5);
		object->normal(Vector3(-1, 0, 0));
		object->position(-5, 5, -5);
		object->normal(Vector3(-1, 0, 0));
		object->position(-5, -5, 5);
		object->normal(Vector3(-1, 0, 0));
	}

	{
		//RIGHT

		//Triangle 1
		object->position(5, 5, -5);
		object->normal(Vector3(1, 0, 0));
		object->position(5, -5, 5);
		object->normal(Vector3(1, 0, 0));
		object->position(5, -5, -5);
		object->normal(Vector3(1, 0, 0));

		//Triangle 2
		object->position(5, 5, -5);
		object->normal(Vector3(1, 0, 0));
		object->position(5, 5, 5);
		object->normal(Vector3(1, 0, 0));
		object->position(5, -5, 5);
		object->normal(Vector3(1, 0, 0));
	}
	object->end();

	node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(object);
}

bool TutorialApplication::frameStarted(const FrameEvent &evt)
{
	
	int accel = 0;
	accel = mAcceleration;
	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		node->translate((10 + mAcceleration) * evt.timeSinceLastFrame, 0, 0);
		mAcceleration++;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		node->translate((-10 - mAcceleration)* evt.timeSinceLastFrame, 0, 0);
		mAcceleration++;
	}

	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		node->translate(0, (10 + mAcceleration)* evt.timeSinceLastFrame, 0);
		mAcceleration++;
	}


	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		node->translate(0, (-10 - mAcceleration)* evt.timeSinceLastFrame, 0);
		mAcceleration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_K)) && (mKeyboard->isKeyDown(OIS::KC_J)))
	{
		node->translate((-10 - mAcceleration) * evt.timeSinceLastFrame, (-10 - mAcceleration) * evt.timeSinceLastFrame, 0);
		mAcceleration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_K)) && (mKeyboard->isKeyDown(OIS::KC_L)))
	{
		node->translate((10 + mAcceleration)* evt.timeSinceLastFrame, (-10 - mAcceleration)* evt.timeSinceLastFrame, 0);
		mAcceleration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_I)) && (mKeyboard->isKeyDown(OIS::KC_J)))
	{
		node->translate((-10 - mAcceleration)* evt.timeSinceLastFrame, (10 + mAcceleration)* evt.timeSinceLastFrame, 0);
		mAcceleration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_I)) && (mKeyboard->isKeyDown(OIS::KC_L)))
	{
		node->translate((10 + mAcceleration)* evt.timeSinceLastFrame, (10 + mAcceleration)* evt.timeSinceLastFrame, 0);
		mAcceleration++;
	}

	if (mKeyboard->isKeyDown(OIS::KC_))
	{
		Degree rotation = Degree(30);
		node->rotate(Vector3(0, 1, 0), Radian(rotation));
	}

	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
